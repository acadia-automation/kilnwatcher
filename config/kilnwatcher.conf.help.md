# Syslog Configuration

This file is used to direct the syslog output for the kilnwatcher service to a
specific file. Without executing this procedure, kilnwatcher logging will be
directed to `/var/log/daemon.log` and mixed with everything else there.

First, ensure that the systemd service entry for kilnwatcher
(`kilnwatcher.service`) has been installed and has the following content:

    StandardOutput=syslog
    StandardError=syslog
    SyslogIdentifier=<your program identifier> # without any quote

1.  Place the `kilnwatcher.conf` file in `/etc/rsyslog.d/`

2.  Restart syslog by executing: `sudo systemctl restart syslog`
