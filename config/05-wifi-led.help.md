# WiFi Connection Status LED
This script file is used to process hooks from the ??? system, in order to
display an LED indicator of the WiFi connection status.

The files is installed in `/lib/dhcpcd/dhcpcd-hooks` by the kilnwatcher
installation script. The file is essentially a DHCP hook processor.

Information about this file and discussion on its use can be found on the
Raspberry Pi forums [in this thread](https://forums.raspberrypi.com/viewtopic.php?t=224423)
