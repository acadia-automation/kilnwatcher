# Power Sensor Configuration
The power sensor configuration file as well as the steps to update the configuration of the power sensor board are described [here on Lechacal's website](http://lechacal.com/wiki/index.php?title=Attiny_Over_Serial_Configuration_A2)

## Configuration Options

The settings below (and that are found in the rpict.conf file in this directory)
are briefly described according to how they are configured for this particular
installation.

* `format = 3` Default format for Emonhub, space separated values

* `nodeid = 11` Default ID for the power sensor board, should differ if multiple
  boards are used

* `polling = 250` Polling interval in milliseconds, chose a sub-second interval
  as the watcher will poll every second or two

* `kcal = 83.33 83.33 83.33 1.0 1.0 1.0 1.0 124.0` Default calibration settings,
  except for the last one which is tuned to adjust for 120v, rather than 240v

* `phasecal = 0` Default setting (no idea what this is for)

* `vest = 1.0` Default setting, though it's important to have it at 1 to get an
  Amp (Irms) reading from the sensor

* `xpFREQ = 60` Updated from default to account for US 60Hz AC

* `Ncycle = 20` Default setting

* `debug = 32` Updated from default to eliminate sensor 3 from the readings. To
  re-include it, set `debug = 0`


## Instructions for updating the configuration

Execute the following command, substituting the path to the rpict.conf file in
this directory:

    lcl-rpict-config.py -a -w [configFile.conf]

The `lcl-rpict-config.py` tool can be downloaded by executing the following
command and unzipping the downloaded file. (Note: this script relies on the apt
package "python-serial"):

    wget lechacal.com/RPICT/tools/lcl-rpict-config.py.zip
