# Automatic Configuration
Various configuration files for the installation and running of the kilnwatcher
service. Each file in the config directory is accompanied by a .help file that
explains it's use and content. These files are all installed by the klinwatcher
installation scripts.

* __05-wifi-led__ - config for enabling wifi connection indicator

* __99-usb.rules__ - udev rules configuration for reliably identifying the
  microcontroller device in the `/dev` directory

* __kilnwatcher_config.yaml__ - This is the configuration override file for the
  kilnwatcher service. The file is documented internally, so there is no .help
  file for this one. Any changes to this file require the service to be
  restarted before they will take effect.

* __kilnwatcher.conf__ - configuration of the system logger for the kilnwatcher
  service

* __kilnwatcher.service__ - the systemd service configuration file

* __rpict.conf__ - used for configuration of the lechacal power sensing device
