# udev rules document

Sometimes, the microcontroller will have a different name for it's device (Ex.,
`/dev/ttyACM1` v `/dev/ttyACM2`) when the RPi starts. udev rules can configure a
statically named symlink to the actual device. To do this, use the command:

`udevadm info -a -n /dev/ttyACM0`

on the device (ttyACM0) to find a unique attribute or combination of attributes
to identify the device with. From there, create a rules file in the
`/etc/udev/rules.d/` directory with an entry that refers to the attribute you've
chosen to identify the device.

In the case of the RP2040-based ItsyBitsy microcontroller from Adafruit, the
second serial device can be identified with the following attribute.

`SUBSYSTEM=="tty", ATTRS{interface}=="CircuitPython CDC2 control", SYMLINK+="kw_micro"`

More detailed instructions can be found in a number of places, but
[these were helpful](http://www.joakimlinde.se/microcontrollers/arduino/avr/udev.php)
as I was figuring this out.
