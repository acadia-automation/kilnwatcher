# systemd Service for Kilnwatcher

This file defines the systemd service that will be used to execute the
Kilnwatcher service. It will handle automatic restarting of the system.

In particular, the kilnwatcher service will execute the `run_kilnwatcher.sh`
script to start the service. All stdout and stderr output is directed to the
syslog service. Configuration for kilnwatcher in syslog is elsewhere in this
configuration directory and should be handled before starting this service.

1.  Place the file into: `/etc/systemd/system/`

2.  Enable systemctl to see the service:

        sudo systemctl daemon-reload

3.  Enable the service so that it doesn’t get disabled if the server restarts:

        sudo systemctl enable kilnwatcher.service

4.  Start the service:

        sudo systemctl start kilnwatcher.service

5.  Stop the service:

        sudo systemctl stop kilnwatcher.service
