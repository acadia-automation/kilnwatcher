#!/bin/bash

# SPDX-FileCopyrightText: Copyright 2022 Ronnie Angerer
# SPDX-License-Identifier: GPL-3.0-or-later

# Global configuarion
INSTALL_LOC="/usr/local"
KILNWATCHER_DIR="src/kilnwatcher"
KILNWATCHER_START_MODULE="start_kiln_watcher.py"
CONFIG_DIR="etc/kilnwatcher"
CONFIG_FILE="kilnwatcher_config.yaml"

# calculate some useful paths
starting_dir=$PWD
kilnwatcher_path="${INSTALL_LOC}/${KILNWATCHER_DIR}"
config_path="${INSTALL_LOC}/${CONFIG_DIR}/${CONFIG_FILE}"

# Verify Install Path
if [[ ! -d "${kilnwatcher_path}" ]]; then
  echo "Kilnwatcher not installed. Halting launch."
  exit 1
fi

# Verify config file
if [[ ! -f "${config_path}" ]]; then
  echo "Kilnwatcher config file not installed. Halting launch."
  exit 2
fi

cd ${kilnwatcher_path}
pipenv run python ${KILNWATCHER_START_MODULE} -c ${config_path}
cd ${starting_dir}
