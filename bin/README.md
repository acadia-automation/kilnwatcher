## Binaries
Executables for installing and running the kilnwatcher service. Both of these
scripts rely on Python 3.x and Pipenv to be installed in the target environment.

* __install.sh__ - This re-runnable installer places the files from
  the config directory into their appropriate locations if they do not already
  exist. Additionally, it copies to the kilnwatcher python package to the
  install directory and installs the appropriate python dependencies. NOTE:
  install locations are defined directly in the install script and can be
  modified there. Execute this script with bash after cloning the repository and
  validating the installation locations in this file.

* __run_kilnwatcher.sh__ - The executable that should be used to start the
  kilnwatcher service. Installation should be run prior to using this executable
  or starting the service. This file can be run directly and is also used by the
  sevice definition.
