#!/bin/bash

# SPDX-FileCopyrightText: Copyright 2022 Ronnie Angerer
# SPDX-License-Identifier: GPL-3.0-or-later

# Source locations
# REVIEW BEFORE INSTALL: Set this appropriately
TEMP_DIR="/tmp"
BASH="/bin/bash"
CURL="/usr/bin/curl"
TAR="/usr/bin/tar"
# These shouldn't change unless the source repository does
INSTALL_ARCHIVE_BASE_URL="http://40newthings.com/kilnwatcher"
CURL_OPTS="-fsSL --output"
INSTALL_ARCHIVE_CONFIG="kilnwatcher-config.tar.gz"
KILNWATCHER_CONFIG_ROOT="kilnwatcher-config"
DAEMON_INSTALL_SCRIPT="install_daemon.sh"
MICROCONTROLLER_INSTALL_SCRIPT="install_microcontroller.sh"

# Install archive sub-paths
SRC_CONFIG_DIR="config"
SRC_BIN_DIR="bin"


# Target locations
# REVIEW BEFORE INSTALL: Set these appropriately
INSTALL_LOC="/usr/local"
TRGT_BIN_DIR="bin"
# These shouldn't change unless the source repository does
STARTUP_FILE="run_kilnwatcher.sh"
SYSTEMD_CONFIG_DIR="/etc/systemd/system"
SYSTEMD_CONFIG_FILE="kilnwatcher.service"
SYSLOG_CONFIG_DIR="/etc/rsyslog.d"
SYSLOG_CONFIG_FILE="kilnwatcher.conf"
DHCPCD_HOOKS_DIR="/lib/dhcpcd/dhcpcd-hooks"
DHCPCD_HOOKS_FILE="05-wifi-led"
UDEV_RULES_DIR="/etc/udev/rules.d"
UDEV_RULES_FILE="99-usb.rules"


# precompile some useful variable
starting_dir=$PWD

download_cmd="${CURL} ${CURL_OPTS} ${TEMP_DIR}/${INSTALL_ARCHIVE_CONFIG} ${INSTALL_ARCHIVE_BASE_URL}/${INSTALL_ARCHIVE_CONFIG}"

download_daemon_install_cmd="${CURL} ${CURL_OPTS} ${TEMP_DIR}/${DAEMON_INSTALL_SCRIPT} ${INSTALL_ARCHIVE_BASE_URL}/${DAEMON_INSTALL_SCRIPT}"
download_microcontroller_install_cmd="${CURL} ${CURL_OPTS} ${TEMP_DIR}/${MICROCONTROLLER_INSTALL_SCRIPT} ${INSTALL_ARCHIVE_BASE_URL}/${MICROCONTROLLER_INSTALL_SCRIPT}"


install_source_path="${TEMP_DIR}/${KILNWATCHER_CONFIG_ROOT}"

startup_script_source_path="${install_source_path}/${SRC_BIN_DIR}/${STARTUP_FILE}"
startup_script_target_path="${INSTALL_LOC}/${TRGT_BIN_DIR}/${STARTUP_FILE}"

service_source_path="${install_source_path}/${SRC_CONFIG_DIR}/${SYSTEMD_CONFIG_FILE}"
service_target_path="${SYSTEMD_CONFIG_DIR}/${SYSTEMD_CONFIG_FILE}"

syslog_source_path="${install_source_path}/${SRC_CONFIG_DIR}/${SYSLOG_CONFIG_FILE}"
syslog_target_path="${SYSLOG_CONFIG_DIR}/${SYSLOG_CONFIG_FILE}"

dhcpcd_source_path="${install_source_path}/${SRC_CONFIG_DIR}/${DHCPCD_HOOKS_FILE}"
dhcpcd_target_path="${DHCPCD_HOOKS_DIR}/${DHCPCD_HOOKS_FILE}"

udev_source_path="${install_source_path}/${SRC_CONFIG_DIR}/${UDEV_RULES_FILE}"
udev_target_path="${UDEV_RULES_DIR}/${UDEV_RULES_FILE}"

echo ""
echo "Fetching Kilnwatcher Config install files..."

# download the installation archive to tmp
${download_cmd}

# unpack the install archive
if [[ -d "${install_source_path}" ]]; then
  echo "    -- Previous Kilnwatcher Config installation left install files. Overwriting..."
else
  mkdir ${install_source_path}
  echo "    -- Created install directory: ${install_source_path}"
fi

cd ${install_source_path}
${TAR} -zxf ${TEMP_DIR}/${INSTALL_ARCHIVE_CONFIG}


if [[ $(systemctl list-units --all -t service --full --no-legend "${SYSTEMD_CONFIG_FILE}" | cut -f1 -d' ') == ${SYSTEMD_CONFIG_FILE} ]]; then
  echo ""
  echo "Stopping Kilnwatcher service."
  sudo systemctl -q stop kilnwatcher.service
fi


# Install systemd service config file if it's not already there
if [[ -f "${service_target_path}" ]] && diff ${service_target_path} ${service_source_path} > /dev/null
then
  echo "Systemd service config file already exists and is identical to the source file."
  echo "    -- No update to ${service_target_path}"
else
  echo "Systemd service config file does not exist or needs update."
  sudo cp ${service_source_path} ${service_target_path}
  echo "    -- Installed config: ${service_target_path}"
  echo "    -- Enabling kilnwatcher service"
  sudo systemctl daemon-reload
  sudo systemctl enable kilnwatcher.service
  echo "    -- Service enabled. To start the kilnwatcher service, execute:"
  echo "    --   sudo systemctl start kilnwatcher.service"
fi
echo ""

# Install syslog config file if it's not already there
if [[ -f "${syslog_target_path}" ]] && diff ${syslog_target_path} ${syslog_source_path} > /dev/null
then
  echo "Syslog config file already exists and is identical to the source file."
  echo "    -- No update to ${syslog_target_path}"
else
  echo "Syslog config file does not exist or needs update."
  sudo cp ${syslog_source_path} ${syslog_target_path}
  echo "    -- Installed config: ${syslog_target_path}"
  echo "    -- Restarting syslog to enable changes..."
  sudo systemctl restart syslog
fi
echo ""

# Install WiFi LED Config file if it's not already there
if [[ -f "${dhcpcd_target_path}" ]] && diff ${dhcpcd_target_path} ${dhcpcd_source_path} > /dev/null
then
  echo "DHCPCD hooks file already exists and is identical to the source file."
  echo "    -- No update to ${dhcpcd_target_path}"
else
  echo "DHCPCD hooks file does not exist or needs update."
  sudo cp ${dhcpcd_source_path} ${dhcpcd_target_path}
  echo "    -- Installed config: ${dhcpcd_target_path}"
  echo "    -- Raspberry Pi will require reboot for wifi changes to take effect..."
fi
echo ""

# Install udev rules file if it's not already there
if [[ -f "${udev_target_path}" ]] && diff ${udev_target_path} ${udev_source_path} > /dev/null
then
  echo "udev rules file already exists and is identical to the source file."
  echo "    -- No update to ${udev_target_path}"
else
  echo "udev rules file does not exist or needs update."
  sudo cp ${udev_source_path} ${udev_target_path}
  echo "    -- Installed config: ${udev_target_path}"
  echo "    -- Raspberry Pi will require reboot for udev changes to take effect..."
fi
echo ""

# Install kilnwatcher run script
sudo cp -f ${startup_script_source_path} ${startup_script_target_path}
sudo chmod ug+x ${startup_script_target_path}
echo "Installed the kilnwatcher startup script to: ${startup_script_target_path}"


echo ""
echo ""
echo "Installing Kilnwatcher daemon software..."
echo "    -- Downloading Daemon installation script ${DAEMON_INSTALL_SCRIPT}"
${download_daemon_install_cmd}
echo "    -- Executing Daemon installation script ${TEMP_DIR}/${DAEMON_INSTALL_SCRIPT}"
${BASH} ${TEMP_DIR}/${DAEMON_INSTALL_SCRIPT}

echo ""
echo ""
echo "Installing Kilnwatcher microcontroller software..."
echo "    -- Downloading Daemon installation script ${MICROCONTROLLER_INSTALL_SCRIPT}"
${download_microcontroller_install_cmd}
echo "    -- Executing Daemon installation script ${TEMP_DIR}/${MICROCONTROLLER_INSTALL_SCRIPT}"
${BASH} ${TEMP_DIR}/${MICROCONTROLLER_INSTALL_SCRIPT}


# Clean up installation files
echo "Removing config installation files..."
rm -Rf ${install_source_path}
rm ${TEMP_DIR}/${INSTALL_ARCHIVE_CONFIG}
rm ${TEMP_DIR}/${DAEMON_INSTALL_SCRIPT}
rm ${TEMP_DIR}/${MICROCONTROLLER_INSTALL_SCRIPT}


echo "Installation complete."
echo ""
