# Manual Configuration
This directory contains configuration that must be manually applied to the
Raspberry Pi prior to running the kilnwatcher.

* __config.txt__ - This file is a full copy of the config.txt file in the
  `/boot` directory of the pi. It is probably safest to just copy the custom
  overlays at the bottom of the file into the existing Pi, since the rest of
  this file may change over time with the progression of the RPi OS.
