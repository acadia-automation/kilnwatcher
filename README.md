# kilnwatcher - Kiln Watcher

![Kilnwatcher Logo](https://gitlab.com/acadia-automation/kilnwatcher/-/raw/main/artwork/kilnwatcher_icon.png "Watching")

## Kiln Watcher
The Kiln Watcher is the software side of a device that tracks various metrics related to the firing of an electric pottery kiln. Some metrics are displayed directly on the Kiln Watcher device, primarily, these function as immediate information for someone approaching the kiln and are intended as additional safety precautions. All metrics are sent to a time series database (Influx) for inspection and analysis during and after the firing.

Examples of metrics that are tracked by the Kiln Watcher:
* The power consumption of the kiln
* Ambient air temperature in the kiln vicinity
* External temperature of the kiln
* Internal temperature of the kiln

## Motivation
Mostly this was created out of my curiosity about the behavior and power consumption of my electric pottery kiln. It also functions as additional safety equipment to indicate clearly when the kiln is hot and may be dangerous to touch or approach.

My motivation for publishing information about the kilnwatcher is mostly that I hope that others might find useful some of the techniques I used to solve problems with various parts of this project. It has practical implementations of solutions that were often described online, but for practical reasons, had trivial examples.

## Build status
Currently, there is a build process using Gitlab CI that generates an installation package and deploys it to the [40 New Things website](http://40newthings.com) for distribution. Installation instructions are included in this document.

## Code style
* numpy style doc comments - though this evolved a little over time. Sorry, but they should all be readable.

## In Pictures
<img src="https://gitlab.com/acadia-automation/kilnwatcher/-/raw/main/artwork/kilnwatcher_panel.jpg" alt="Kilnwatcher Physical Panel" width="200"/>

This is the main panel where the bulk of the electronics for the Kiln Watcher are housed.

## Physical equipment used
* [Raspberry Pi Zero](https://www.raspberrypi.org/products/raspberry-pi-zero/) - Using the Zero without the built-in WiFi, since the device is housed in a metal enclosure. Instead, network access is provided with a USB-attached WiFi client device.
* Adafruit [ItsyBitsy RP2040](https://www.adafruit.com/product/4888) - This is a tiny CircuitPython-capable board with more than enough I/O and sufficient RAM to meet the needs of this project. If I were starting out with this board, likely all of the hardware would be handled with it. As it stands, late in the build, I changed from Arduino/C++ to CircuitPython and needed more memory than the [Trinket MO](https://www.adafruit.com/product/3500) provided. Connected to the RPi using a serial communication over a USB cable that also provides power for the board.
* Lechacal [3v1 Power sensor board](http://lechacalshop.com/gb/internetofthing/20-raspberrypi-1x-current-sensor-adaptor-1-voltage-emoncms.html) - There is a version of this that is meant to be a hat for a Pi Zero, but I went with the standard one, as I did not intend to use it as a hat. It would have blocked the GPIOs and I wanted them for other connections.  Connected to the RPi through various GPIOs.
* [AC Current Sensors](https://www.amazon.com/gp/product/B07FZZZ62L/ref=ppx_yo_dt_b_search_asin_title?ie=UTF8&psc=1) - These are Split Core Transformers rated for 100A AC. There is one on each of the 120V conductors that lead to the 240V receptacle that the kiln is plugged into. This is a dedicated, 60A circuit for the kiln. Connected to the power sensor board.
* Adafruit [Infrared Temperature sensor](https://www.adafruit.com/product/1747) - Small and contactless, which is good, as the kiln gets kinda hot.
* Adafruit [SHT30 Temperature and Humidity sensor](https://www.adafruit.com/product/5064) for measuring ambient temp and humidity in the kiln area. This product has a nice little housing for the sensor. Connected to the microcontroller with i2c.
* Adafruit [MCP9600 Thermocouple Amplifier](https://www.adafruit.com/product/4101) - Used to read the signal from the thermocouple below. Connected to the microcontroller with i2c.
* [Type K "Pyrocil" thermocouple](https://hotkilns.com/kiln-parts/type-k-pyrocil-sheathed-thermocouple-5-long) from L&L Kilns. This measures the temperature inside of the kiln and is connected to the microcontroller via the amplifier breakout board, above.
* Adafruit [Neopixel Jewel](https://www.adafruit.com/product/2858) - Used to light up the indicators. I found some old panel mount lamp housings on ebay that I retrofitted with these leds. Connected to the microcontroller with i2c.
* Adafruit [7 Segment display with Backback](https://www.adafruit.com/product/878) - Basic displays for temperature on the panel. Connected to the RPi with i2c.
* Fancy, little [brass momentary switch](https://www.etsy.com/listing/856246341/momentary-switch-8mm) from [KalymianCustomSabers](https://www.etsy.com/shop/KalymianCustomSabers?ref=simple-shop-header-name&listing_id=856246341), who makes models (and parts for) light sabers. Overkill for my needs, but it does look quite nice.
* Adafruit [Proto boards](https://www.adafruit.com/product/4785) - Used these for wiring up the various components to keep things neater than wires all over the place.
* [pc817 Optocouplers](https://www.amazon.com/gp/product/B07K1PW21P/ref=ppx_yo_dt_b_search_asin_title?ie=UTF8&psc=1) - For isolating the kiln's vent fan signal from the Kiln Watcher electronics, but still allowing the ability to monitor when the fan (and thus the kiln) is on and off.
* Various other electronic components and parts that are fairly standard and I'm not going to list those here (wire, header pins, resistors, etc).

## Features
[What makes your project stand out?]

## Installation
### Prerequisites
* Running Raspberry Pi
* RPi i2c and smb enabled (raspi-config helps)
* Python 3+
* Pipenv

### Process
First, review the "config_manual" directory in this project for manual configuration steps that need to happen with the Raspberry Pi, prior to running the installation process.

From a shell on running on the Raspberry Pi, execute:

    `/bin/bash -c "$(curl -fsSL http://40newthings.com/kilnwatcher/install.sh)"`

The installation script will:
* download the latest install archive ([kilnwatcher_install.tar.gz](http://40newthings.com/kilnwatcher/kilnwatcher_install.tar.gz)), which includes the python package and supporting files from the 40newthings/kilnwatcher location.
* extract the archive to /tmp
* copy python, execution, and configuration files to appropriate locations inside of /usr/local and /etc - if an existing python package is found, it will be moved into an archived location under /usr/local/src
* load the kilnwatcher service into system.d
* configure kilnwatcher with syslog to output a kilnwatcher specific log to /var/log/kilnwatcher.log
* cleans up installation files

*Security Note:* This script requires sudo access and uses the elevated permissions for system.d configuration and for writing to the /usr/local directories.

If you would like to review or change the installation script prior to running it, just [download it to your system](http://40newthings.com/kilnwatcher/install.sh) and open with your favorite text editor.

## Tests
Unit tests are available for the Python code in the Kilnwatcher Daemon and Kilnwatcher Microcontroler submodules. See that module for more detail.

## How to use?
To start the kilnwatcher service, execute:

    `sudo systemctl start kilnwatcher.service`

## Contribute
Not looking for contribution at this time.

## Credits
Much credit goes to the amazing folks over at Adafruit and the CircuitPython community. The learning resources, the electronic components, and the included documentation and usage resources are the best I've found.

Elsewhere in this project, I have made an effort to reference and attribute credit to the resources I found online for the various parts of this solution.

## License
TODO
